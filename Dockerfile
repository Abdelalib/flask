FROM python:3.8-slim-buster
WORKDIR /opt
RUN apt-get update --fix-missing
COPY src/requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
COPY src/run.py .
COPY src/app ./app
EXPOSE 80
CMD ["gunicorn", "-b", "0.0.0.0:80", "run:app"]
